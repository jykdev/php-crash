<?php

/* ---------- File Handling --------- */

/* 
  File handling is the ability to read and write files on the server.
  PHP has a built in functions for reading and writing files.
*/

$file = '../extras/users.txt';

if(file_exists($file)) {
  echo readfile($file), '<br>';

  $handle = fopen($file, 'r'); // this acts like a pointer to the file location
  $contents = fread($handle, filesize($file));
  fclose($handle);
  echo $contents;
} else {
  $handle = fopen($file, 'w');
  $contents = 'john' . PHP_EOL . 'sara' . PHP_EOL . 'mike';
  fwrite($handle, $contents);
  fclose($handle);
  echo $contents;
}

?>