<?php

/* ---- Conditionals / Control Structures & Operators ---- */

/* ------------ Operators ----------- */

/*
  < Less than
  > Greater than
  <= Less than or equal to
  >= Greater than or equal to
  == Equal to
  === Identical to
  != Not equal to
  !== Not identical to
*/

/* ---------- If & If-Else Statements --------- */

/*
** If Statement Syntax
if (condition) {
  // code to be executed if condition is true
}
*/

$age = 20;

if($age >= 18) {
  echo 'You are old enough to vote';
} else {
  echo 'Sorry, you are not old enough to vote';
};
echo '<br>';

$t = date('H');
echo $t;
echo '<br>';

if($t < 12) {
  echo 'Good morning';
} elseif($t <17) {
  echo 'Good afternoon';
} else {
  echo 'Good evening';
};
echo '<br>';

$posts = ['First Post', 'Second Post', 'Third Post'];

if(!empty($posts)) {
  echo $posts[0];
} else {
  echo 'No posts';
}
echo '<br>';
echo '<br>';


/* -------- Ternary Operator -------- */
/*
The ternary operator is a shorthand if statement.
Ternary Syntax:
condition ? true : false;
*/

echo 'Ternary Operator';
echo '<br>';

echo !empty($posts) ? $posts[0] : 'No posts';
echo '<br>';

$firstPost = !empty($posts) ? $posts[0] : 'No posts';
echo $firstPost;
echo '<br>';

// If there's no 'else', you can use 'null'
$secondPost = !empty($posts) ? $posts[1] : null;
echo $secondPost;
echo '<br>';

// If there's no 'else', you can use alternatively use a coalescing operator'
$thirdPost = $posts[2] ?? null;
echo $thirdPost;
echo '<br>';

/* -------- Switch Statements ------- */
// If you have a bunch of elseif's, it might be helpful to use a switch instead

$fav_color = 'yellow';
switch($fav_color) {
  case 'red':
    echo 'Your favorite color is red';
    break;
  case 'blue';
    echo 'Your favorite color is blue';
    break;
  case 'green';
    echo 'Your favorite color is green';
    break;
  default:
    echo 'Your favorite color is not red, green, or blue';
};
echo '<br>';


echo '<br>';
echo '<br>';
echo '<br>';
