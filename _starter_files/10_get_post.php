<?php
/* --- $_GET & $_POST Superglobals -- */

/*
  We can pass data through urls and forms using the $_GET and $_POST superglobals.

  $_GET can get data through both urls and forms. Only thing where GET might be used is for a search where you're not actually submitting any data

  $_POST can get data through forms only.  POST is more secure than GET
*/

if(isset($POST['submit'])) {
  echo $POST['name'];
  echo $POST['age'];
} // used to prevent error messages when there no values yet for the variables b/c nothing has been submitted yet

?>


<a href="<?php echo $_SERVER['PHP_SELF']; ?>">Click</a>  <!-- link to page this page to itself -->
<br>

<a href="<?php echo $_SERVER['PHP_SELF']; ?>?name=John&age=30">Click</a> <!-- this is called a query string -->
<br>

<?php echo $_POST['name']; ?>
<br>
<?php echo $_POST['age']; ?>
<br>

<!-- action is the file that we want to submit the form to -->
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
  <div>
    <label for="name">Name: </label>
    <input type="text" name="name" id="">
  </div>
  <div>
    <label for="age">Age: </label>
    <input type="text" name="age" id="">
  </div>
  <input type="submit" value="Submit" name="submit">
</form> 


