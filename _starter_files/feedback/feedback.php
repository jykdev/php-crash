<?php include 'inc/header.php'; ?>

<?php 
  
  // $feedback = [
  //   [
  //     'id' => '1',
  //     'name' => 'Beth Williams',
  //     'email' => 'beth@gmail.com',
  //     'body' => 'T media is great',
  //   ],
  //   [
  //     'id' => '2',
  //     'name' => 'Jack Rhino',
  //     'email' => 'jack@gmail.com',
  //     'body' => 'T media is wonderful',
  //   ],
  //   [
  //     'id' => '3',
  //     'name' => 'John Doe',
  //     'email' => 'john@gmail.com',
  //     'body' => 'T media is just okay',
  //   ],
  // ];

  // More advanced form: OOP can have special classes / opbjects for instance could have a feedback and use feedback get to get the data. Below is for absolute beginners
  $sql = 'SELECT * FROM feedback';
  $result = mysqli_query($conn, $sql);
  $feedback = mysqli_fetch_all($result, MYSQLI_ASSOC);
  
?>


<h2>Past Feedback</h2>

<!-- check if there is feedback data that exists -->
<?php if(empty($feedback)): ?>
<!-- TODO: how does this line work? -->

<p class="lead mt3">There is no feedback</p>

<?php endif; ?>

<?php foreach($feedback as $item): ?>
<div class="card my-3 w-75">
  <div class="card-body text-center">
    <?php echo $item['body']; ?>
    <div class="text-secondary mt-2">
      By <?php echo $item['name']; ?> on <?php echo $item['date']; ?>
    </div>
  </div>
</div>
<?php endforeach; ?>

<?php include 'inc/footer.php'; ?>