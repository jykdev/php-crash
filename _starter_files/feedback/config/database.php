<!-- // TODO: using mysqli below, but learn to how to us PDO b/c it supports more sql formats -->

<?php 
  define('DB_HOST', 'localhost');
  define('DB_USER', 'john');
  define('DB_PASS', 'john');
  define('DB_NAME', 'php_dev');

  // create connection
  $conn = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME); // instantiating a new mysql connection

  // check connection
  if($conn->connect_error) {  //TODO: what is connect_error property all about?
    die('Connection Failed' . $conn->connect_error); // TODO: what does the die function do?
  }

// echo 'CONNECTED!';