<?php 
/* --- Object Oriented Programming -- */

/*
  From PHP5 onwards you can write PHP in either a procedural or object oriented way. OOP consists of classes that can hold "properties" and "methods". Objects can be created from classes.
*/

class User {
  // Properties are attributes that belong to a class

  // Access Modifiers: public, private, protected
  // public - can be accessed from anywhere
  // private - can only be accessed from inside the class
  // protected - can only be accessed from inside the class and by inheriting classes

  public $firstName;
  public $email;
  public $password;

  // A constructor is a method that runs when an object is created
  public function __construct($firstName, $email, $password) {
    $this->firstName = $firstName;
    $this->email = $email;
    $this->password = $password;
  }


  // method is a function that belongs to a class --> setter method type
  function set_name($firstName) {
    $this->firstName = $firstName; // firstName comes from the class firstName property.  the class property does not have to be the same as the method variable (i.e. $name), but they should be
  }
  
  ////// method is a function that belongs to a class --> setter method type
  function get_name() {
    return $this->firstName;
  }
}


////// Instantiate a user object
$user1 = new User('John', 'john@gmail.com', '12321');
$user2 = new User('Jack', 'jack@gmail.com', '23123');

// $user1->firstName = 'John'; // set a property in the $user1 object called firstName
$user1->set_name('John'); // set a method of the $user1 object called set_name
$user2->set_name('Jack'); // set a method of the $user1 object called set_name

// var_dump($user1);
// var_dump($user2);

echo $user1->email, '<br>';
echo $user2->firstName, '<br>';

////// Inheritance
class Employee extends user {
  public function __construct($firstName, $email, $password, $title) {
    parent::__construct($firstName, $email, $password);
    $this->title = $title;
  } 
  public function get_title() {
    return $this->title;
  }
}

////// Instantiate
$employee1 = new Employee('Sara', 'sara@gmail.com', '123123', 'Manager');
echo $employee1->get_title(), '<br>';