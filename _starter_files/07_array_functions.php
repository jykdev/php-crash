<?php
/* --------- Array Functions -------- */

/*
  Functions to work with arrays
  https://www.php.net/manual/en/ref.array.php
*/

////////////////////////////////
echo '<br>';
echo '/////ARRAY FUNCTIONS';
echo '<br>';
echo '<br>';
$fruits = ['apple', 'orange', 'pear', 'banana', 'lemon'];


// Get length
echo 'GET LENGTH <br>';
echo count($fruits);
echo '<br>';
echo '<br>';

// Search array for a specific value with return a boolean 
echo 'SEARCH ARRAY <br>';
echo var_dump(in_array('apple', $fruits));
echo '<br>';
echo '<br>';

// Add to an array to end of array
echo 'ADD TO ARRAY - SQ BRACKETS [END] <br>';
$fruits[] = 'grape';
print_r($fruits);
echo '<br>';
echo '<br>';

// Add to an array to end of array 
echo 'ADD TO ARRAY - ARRAY_PUSH [END] <br>';
array_push($fruits, 'blueberry', 'strawberry'); // append values to end of array
print_r($fruits);
echo '<br>';
echo '<br>';

// Add to an array to beginning of array 
echo 'ADD TO ARRAY - ARRAY_UNSHIFT [BEGINNING] <br>';
array_unshift($fruits, 'mango'); // append value to beginning of array
print_r($fruits);
echo '<br>';
echo '<br>';

// Remove from array will take values off the end   
echo 'REMOVE FROM ARRAY - POP [END] <br>';
print_r(array_pop($fruits));
echo '<br>';
print_r($fruits);
echo '<br>';
echo '<br>';

// Remove from array will take values off the beginning   
echo 'REMOVE FROM ARRAY - SHIFT [BEGINNING] <br>';
print_r(array_shift($fruits));
echo '<br>';
print_r($fruits);
echo '<br>';
echo '<br>';

// Remove specific value from the array
echo 'REMOVE SPECIFIC VALUE FROM ARRAY - UNSET <br>';
unset($fruits[2]); // unset will also remove the index such that the 2 index is removed
print_r($fruits);
echo '<br>';
echo '<br>';

// Split array into chunks of two values for each chunked array
echo 'CHUNK ARRAYS <br>';
$chunked_array = array_chunk($fruits, 2);
print_r($chunked_array);
echo '<br>';
echo '<br>';

// Merge multiple arrays into one array using array_merge
echo 'MERGE ARRAYS - ARRAY_MERGE <br>';
$arr1 = [1, 2, 3];
$arr2 = [4, 5, 6];
$arr3 = array_merge($arr1, $arr2);
print_r($arr1);
echo '<br>';
print_r($arr2);
echo '<br>';
print_r($arr3);
echo '<br>';
echo '<br>';

// Merge multiple arrays into one array using spread operator
echo 'MERGE ARRAYS - SPREAD OPERATOR <br>';
$arr4 = [7, 8, 9];
$arr5 = [10, 11, 12];
$arr6 = [...$arr4, ...$arr5];
print_r($arr4);
echo '<br>';
print_r($arr5);
echo '<br>';
print_r($arr6);
echo '<br>';
echo '<br>';

// using array_combine(), create a PHP associative array (i.e. JS object, Python dictionary)
echo 'MERGE ARRAYS - SPREAD OPERATOR <br>';
$a = ['green', 'red', 'yellow'];
$b = ['avacado', 'apple', 'banana'];
$c = array_combine($a, $b);
print_r($c);
echo '<br>';

$keys = array_keys($c); // create an array of keys from a PHP associative array
print_r($keys);
echo '<br>';

$flipped = array_flip($c); // from an associative array, flip the key/value pairs
print_r($flipped);
echo '<br>';
echo '<br>';

// using array_map(), to send each value of an array to a function
echo 'MAP ARRAYS - PASS EACH ARRAY VALUE TO A FUNCTION <br>';
$numbers = range(1, 5);
print_r($numbers);
echo '<br>';

$newNumbers = array_map(function($number) {
  return "Number $number";
}, $numbers);
print_r($newNumbers);
echo '<br>';
echo '<br>';

// using array_filter(), to send each value of an array to a function
echo 'FILTER ARRAYS - PASS FILTERED ARRAY VALUES TO A FUNCTION <br>';
$lessThanTen = array_filter($numbers, fn($number) => $number <= 3);
print_r($lessThanTen);
echo '<br>';
echo '<br>';

// using array_reduce(), reduces the array to a single value (i.e. sum)
echo 'REDUCE ARRAYS - SUM VALUES IN AN ARRAY <br>';
$sumArrayWithReduce = array_reduce($numbers, fn($carry, $number) => $carry + $number);
print_r($sumArrayWithReduce);
echo '<br>';
var_dump($sumArrayWithReduce);
echo '<br>';
echo '<br>';



