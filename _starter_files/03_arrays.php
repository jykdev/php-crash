<?php
/* ----------- Arrrays ----------- */

/*
  Arrays hold multiple values. Each value in an array is called an "element"
*/

//// SIMPLE ARRAY
$numbers = [1, 44, 55, 22];
$fruits = array('apple', 'oranges', 'pear');

print_r($numbers);
echo '<br>';

var_dump($numbers);
echo '<br>';

echo $fruits[1];
echo '<br>';

//// ASSOCIATIVE ARRAY
$hex = [
  'red' => '#f00',
  'blue' => '#0f0',
  'green' => '#00f',
];

echo $hex['blue'];
echo '<br>';

$person = [
  'first_name' => 'Brad',
  'last_name' => 'Traversy',
  'email' => 'brad@gmail.com',
];

echo $person['first_name'];
echo '<br>';


//// MULTI-DIMENSIONAL ARRAYS
$people = [
  [
    'first_name' => 'Brad',
    'last_name' => 'Traversy',
    'email' => 'brad@gmail.com',
  ],
  [
    'first_name' => 'John',
    'last_name' => 'Doe',
    'email' => 'john@gmail.com',
  ],
  [
    'first_name' => 'Jane',
    'last_name' => 'Doe',
    'email' => 'jane@gmail.com',
  ]
];

echo $people[1]['email'];
echo '<br>';

//// CONVERT MULTI-DIMENSIONAL ARRAY INTO JSON OUTPUT
var_dump(json_encode($people));


//// CONVERT JSON OUTPUT INTO MULTI-DIMENSIONAL ARRAY
// var_dump(json_decode());


