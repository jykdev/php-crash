<?php
/* ------------ Functions ----------- */

/*
  Functions are reusable blocks of code that we can call to perform a specific task.
  We can pass values into functions to change their behavior. Functions have their own local scope as opposed to global scope


/*
** Function Syntax
  function functionName($arg1, $arg2, ...) {
    // code to be executed
  }
*/

////////////////////////////////
echo 'FUNCTION - BASIC SYNTAX';
echo '<br>';
function registerUser1() {
  echo 'User registered';
} // declaring function

registerUser1(); // calling function
echo '<br>';


////////////////////////////////
echo '<br>';
echo 'FUNCTION - SCOPE EXAMPLE';
echo '<br>';

$y = 12; // variables with global scope cannot be called inside function unless there is a global operator
function registerUser2() {
  global $y; // using globa operator to call global variable from inside function
  echo $y, '<br>';
  $x = 10; // variables with function scope cannot be called outside function
  echo 'User registered';
}

// echo $x;  // this variable has a function scope and cannot be called outside the function
registerUser2();


////////////////////////////////
echo '<br>';
echo '<br>';
echo 'FUNCTION - ARGUMENTS AND PARAMETERS';
echo '<br>';
function registerUser3($email) { // $email is the argument
  echo "$email registered";
}

registerUser3('john@aol.com'); // john@aol.com is the parameter when we're passing an object into the function


////////////////////////////////
echo '<br>';
echo '<br>';
echo 'FUNCTION - WITH RETURN VALUE';
echo '<br>';
function sumTwoNumbers($n1, $n2) { // usually when you have a function, you have a return value
  return $n1 + $n2;
}

echo sumTwoNumbers(3, 5); // echo the function to print return value
echo '<br>';

$twoNumbersVariable = sumTwoNumbers(6, 3);
echo $twoNumbersVariable; // store return value in variable and print variable
echo '<br>';


////////////////////////////////
echo '<br>';
echo '<br>';
echo 'FUNCTION - DEFAULT VALUES';
echo '<br>';
function sumDefaultNumbers($n1 = 4, $n2 = 10) { // usually when you have a function, you have a return value
  return $n1 + $n2;
}

echo sumDefaultNumbers(); 
echo '<br>';


////////////////////////////////
echo '<br>';
echo '<br>';
echo 'FUNCTION - ANONYMOUS FUNCTIONS SET TO VARIABLES';
echo '<br>';
$subtractTwoNumbers = function($n1, $n2) {
  return $n1 - $n2;
};

echo $subtractTwoNumbers(12, 10); 
echo '<br>';

////////////////////////////////
echo '<br>';
echo '<br>';
echo 'FUNCTION - ARROW FUNCTIONS';
echo '<br>';
$multiplyTwoNumbers = fn($n1, $n2) => $n1 * $n2;

echo $multiplyTwoNumbers(12, 10); 
echo '<br>';


