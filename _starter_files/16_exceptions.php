<?php

/* ----------- Exceptions ----------- */

/*
  PHP has an exception model similar to that of other programming languages. An exception can be thrown, and caught ("catched") within PHP. Code may be surrounded in a try block, to facilitate the catching of potential exceptions. Each try must have at least one corresponding catch or finally block.
*/

function inverse($x) {
  if(!$x) {
    throw new Exception('Division by zero'); // TODO: how does this line work?
  }
  return 1/$x;
}



// TODO: what are some real world application of the try/finally function model? what is the purpose?
try { 
  echo inverse(5), '<br>';
  echo inverse(0), '<br>';
} catch(Exception $e) {
  echo 'Caught Exception', $e->getMessage(), " ", "<br>";
} finally {
  echo 'First Finally', "<br>";
}


echo "Hello World";