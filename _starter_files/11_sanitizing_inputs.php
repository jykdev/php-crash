<?php
/* --- Sanitizing Inputs -- */

/*
  Data submitted through a form is not sanitized by default. We have methods to sanitize data manually.

  Sanitizing inputs means removing illegal characters using deleting, replacing, encoding, or escaping techniques. PHP provides a list of sanitizing filters that you can use to sanitize input effectively.
*/

//// using htmlspecialchars()
// if(isset($_POST['submit'])) {
//   $name = htmlspecialchars($POST['name']);
//   $age = htmlspecialchars($POST['age']);
//   echo $name;
//   echo $age;
// } 

//// using filter_input()
if(isset($_POST['submit'])) { // TODO: understand $_POST global constant better. How does this link to the form?
  $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_SPECIAL_CHARS); // TODO: understand this filter_input function better and play around with it
  $age = filter_input(INPUT_POST, 'age', FILTER_SANITIZE_SPECIAL_CHARS);
  
  echo $name, '<br>';
  echo $age;
} 

?>

<form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="POST">
  <div>
    <label for="name">Name: </label>
    <input type="text" name="name" id="">
  </div>
  <div>
    <label for="age">Age: </label>
    <input type="text" name="age" id="">
  </div>
  <input type="submit" value="Submit" name="submit">
</form> 