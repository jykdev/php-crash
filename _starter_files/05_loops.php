<?php

/* -------- Loops & Iteration ------- */

/* ------------ For Loop ------------ */

/*
** For Loop Syntax
  for (initialize; condition; increment) {
  // code to be executed
  }
*/

echo 'FOR LOOP';
echo '<br>';
for($x = 0; $x <= 10; $x++) {
  echo "For the number $x";
  echo '<br>';
}

/* ------------ While Loop ------------ */

/*
** While Loop Syntax
while (condition) {
  // code to be executed
}
*/

$y = 1;

echo '<br>';
echo 'WHILE LOOP';
echo '<br>';
while($y <= 15) {
  echo "While the number is $y <br>";
  $y = $y + 1;
}


/* ---------- Do While Loop --------- */

/*
** Do While Loop Syntax
do {
  // code to be executed
} while (condition);

do...while loop will always execute the block of code once, even if the condition is false.
*/

$z = 1;

echo '<br>';
echo 'DO WHILE LOOP';
echo '<br>';
do {
  echo "Do while number is $z <br>";
  $z++;
} while($z <= 5);



/* ---------- Foreach Loop ---------- */

/*
** Foreach Loop Syntax
  foreach ($array as $value) {
  // code to be executed
  }
*/

////// In PHP development will use this more than any other loop b/c constantly working with arrays (data from a database or file)

$posts = ['First post', 'Second post', 'Third post'];

echo '<br>';
echo 'FOR LOOP (ARRAYS)';
echo '<br>';
for($x = 0; $x < count($posts); $x++) {
  echo $posts[$x], '<br>';
}


echo '<br>';
echo 'FOREACH (ARRAYS)';
echo '<br>';
foreach($posts as $post) {
  echo $post, '<br>';
}

echo '<br>';
echo 'FOREACH WITH INDEX (ARRAYS)';
echo '<br>';
foreach($posts as $index => $post) {
  echo "$index - $post <br>";
}


$person = [
  'first_name' => 'Brad',
  'last_name' => 'Traversy',
  'email' => 'brad@gmail.com',
];

echo '<br>';
echo 'FOREACH WITH INDEX (ASSOCIATIVE ARRAYS)';
echo '<br>';
foreach($person as $key => $value) {
  echo "$key - $value <br>";
}


