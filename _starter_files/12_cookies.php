<?php
/* ------------- Cookies ------------ */

/*
  Cookies are a mechanism for storing data in the remote browser and thus tracking or identifying return users. You can set specific data to be stored in the browser, and then retrieve it when the user visits the site again.
*/

// Set a cookie
setcookie('name', 'John', time() + 864000 * 30); // 864000 is the number of seconds in a day

// Use a cookie
if(isset($_COOKIE['name'])) { // isset() checks that the cookie exits first
  echo $_COOKIE['name'];
}

// To delete a cookie
setcookie('name', '', time() - 864000); // setting a cookie to a time in the past deletes the cookie