<?php

/* ----- Variables & Data Types ----- */

/* --------- PHP Data Types --------- */
/*
- String      A string is a series of characters surrounded by quotes
- Integer     Whole numbers
- Float       Decimal numbers
- Boolean     true or false
- Array       An array is a special variable, which can hold more than one value
- Object      A class
- NULL        Empty variable
- Resource    A special variable that holds a resource
*/

/* --------- Variable Rules --------- */
/*
- Variables must be prefixed with $
- Variables must start with a letter or the underscore character
- variables can't start with a number
- Variables can only contain alpha-numeric characters and underscores (A-z, 0-9, and _ )
- Variables are case-sensitive ($name and $NAME are two different variables)
*/

$name = 'Brad'; // string
$age = 40; // int
$has_kids = true; // bool
$cash_on_hand = 20.75; // float

echo $name;
echo '<br>';

echo $age;
echo '<br>';

var_dump($has_kids);
echo '<br>';

var_dump($cash_on_hand);
echo '<br>';

echo $name . ' is ' . $age . ' years old'; // using variables in strings: single quotes and periods
echo '<br>';

echo "$name is $age years old"; // using variables in strings using double quotes
echo '<br>';

$x = 5 + 5;
var_dump($x);
echo '<br>';

$y = '5' + '5'; // can use quotes for numbers and will return an integer, unlike in JS
var_dump($y);
echo '<br>';

$z = 10 - 5;
var_dump($z);
echo '<br>';

$a = 5 * 6;
var_dump($a);
echo '<br>';

$b = 10 / 5;
var_dump($b);
echo '<br>';

$c = 10 % 9; // modulus operator give the remainder
var_dump($c);
echo '<br>';

//// CONSTANTS (key value pairs)
define('HOST', 'localhost'); // convention is to use all CAPS for key
define('DB_NAME', 'dev_db');
echo HOST;
echo '<br>';
