<?php
/* ------------ Sessions ------------ */

/*
  Sessions are a way to store information (in variables) to be used across multiple pages.
  Unlike cookies, which are stored on the browser, sessions are stored on the server.
*/
session_start();

if(isset($_POST['submit'])) { 
  $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_SPECIAL_CHARS); 
  
  $password = $_POST['password']; // don't want to sanitize the password b/c it's not going to be put in a page.  If using a database, would never store a plain text password in a database, you would hash it.
  
  if($username == 'john' && $password == 'password') {
    $_SESSION['username'] = $username;
    header('Location: /php-crash/extras/dashboard.php');
  } else {
    echo 'Incorrect Login';
  }
} 

?>

<form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="POST">
  <div>
    <label for="username">Username: </label>
    <input type="text" name="username" id="">
  </div>
  <div>
    <label for="password">Password: </label>
    <input type="password" name="password" id="">
  </div>
  <input type="submit" value="Submit" name="submit">
</form> 