<?php
/*
  A .php file running on a server, can output both HTML and PHP code. There are multiple functions that can be used to output data to the browser.
*/

// echo - output strings, numbers, html, etc
echo 123, 'Hello', 10.5;
echo '<br>';
echo '<br>';

// print - works like echo, but can only take on a single argument
print 123;
echo '<br>';
echo '<br>';

// print_r() - print single values and arrays
print_r("Hello");
echo '<br>';
print_r([1, 2, 3, 4]);
echo '<br>';
echo '<br>';


// var_dump() - returns more info like data type and length
var_dump('Hello John');
echo '<br>';
var_dump(true);
echo '<br>';
echo '<br>';


// var_export() - similar to var_dump(). Outputs a string representation of a variable.
var_export("Hello");
echo '<br>';
echo '<br>';

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>

<body>
  <h1><?php echo 'Post One'; ?></h1> // Inline PHP syntax
  <h1><?= 'Post One'; ?></h1> // Inline PHP syntax
</body>

</html>